import * as echarts from 'echarts/core'
const SysPrepositionLineAndBar = (data) => {
    let yNameOne = '已完成数量';
    let yNameTwo = '月计划数量';
    let yNameThree = '月达成率'
    // let xData = xVal;
    let xData = ['1月','2月','3月','4月'];
    let yData = {
        one: ['598844', '671280', '1018105',0], //实际
        two: ['770000', '790000', '1230534',0], //计划
        three: [77.77, 84.97, 82.74,0]
    };

    const defaultConfig = {
        tooltip: {
            trigger: 'axis',
            axisPointer: {
                type: 'cross',
                label: {
                    backgroundColor: '#6a7985'
                }
            },
            formatter(params){
                let labelStyle = "display: inline-block; width:80px;padding-left:5px;";
                let numStyle = "display: inline-block;text-align: right;;min-width:100px;padding:1px 0";
                let res = `<div style=""><p style="font-size:14px;margin-bottom: 3px;color:#787878">${params[0].name}</p></div>`;
                let isRate;
                for (var i = 0; i < params.length; i++) {
                    if (params[i].seriesIndex == 2) {
                        isRate = parseFloat(params[i].data).toFixed(2) + '%';
                    } else {
                        isRate = parseFloat(params[i].data);
                    }
                    res += `<p>${params[i].marker}<lable style="${labelStyle}">${params[i].seriesName}</lable><b style="${numStyle}">${isRate}</b></p>`
                }
                return res;
            }
        },
        legend: {
            // data: ['实际产出', '达成率'],
            x: 'center',
            top: 50,
            align: "auto",
            itemGap: 25,
            textStyle: {
                color: 'rgba(255,255,255,.8)',
                fontStyle: "normal",
                fontFamily: 'PingFang SC',
                padding: 3,
                fontSize:14
            },
        },
        grid: {
            top: '26%',
            left: '18%',
            right: '16%',
            bottom: '12%',
        },
        xAxis: {
            type: 'category',
            boundaryGap: true,
            axisTick: { show: false },
            axisLabel: {
                color: 'rgba(255, 255, 255, .8)',
                fontSize:14,
                // interval:1
            },
            axisLine: {
                show: false,
            },
            data: xData,
        },
        yAxis: [
            {
                type: 'value',
                position: 'left',
                min: 0,

                splitLine: {
                    lineStyle: { type: 'dashed', color: 'rgba(31, 81, 124, 1)' },
                },
                axisLabel: {
                    color: 'rgba(255, 255, 255, .8)',
                    fontSize:14
                },
                axisLine: {
                    show: false,
                },
            },
            {
                type: 'value',
                position: 'right',
                min: 0,
                max: 100,
                splitNumber:4,
                interval:20,
                splitLine: { show: false },
                axisLabel: {
                    color: 'rgba(255, 255, 255, .8)',
                    fontSize:14,
                    formatter: '{value}.00%',
                },
                axisLine: {
                    show: false,
                },
            },
        ],
        series: [
            {
                name: yNameOne,
                type: 'bar',
                barWidth:15,
                itemStyle: {
                    normal: {
                        color: new echarts.graphic.LinearGradient(0, 0, 0, 1,
                            [
                                { offset: 1, color: 'rgba(79, 194, 255, .7)' },
                                { offset: 0, color: 'rgba(79, 194, 255, 1)' }
                            ]
                        ),
                    },
                },
                data: yData.one,
            },
            {
                name: yNameTwo,
                type: 'bar',
                barWidth: 15,
                itemStyle: {
                    normal: {
                        color: new echarts.graphic.LinearGradient(0, 0, 0, 1,
                            [
                                { offset: 1, color: 'rgba(200, 255, 56, .7)' },
                                { offset: 0, color: 'rgba(200, 255, 56, 1)' }
                            ]
                        ),
                    },
                },
                data: yData.two
            },
            {
                name: yNameThree,
                type: 'line',
                // symbol: 'circle',
                symbolSize: 8,
                yAxisIndex: 1,
                itemStyle: {
                    normal: {
                        color: 'rgba(248, 160, 54, 1)',
                        label: { // 在折线图显示比率
                            show: true,
                            position: 'top',
                            textStyle: {
                                fontSize: 14,
                                padding: [4,3,3,3],
                                borderRadius: 5,
                                backgroundColor: 'rgba(64, 162, 222, 0.57)',
                                borderColor: 'rgba(79, 194, 255, 1)',
                                color: 'rgba(255, 255, 255, .8)',
                            },
                            formatter: function (params) {
                                // return (Math.round(params.value / 1000 * 10000) / 100.00) + '%';
                                return `${parseFloat(params.value).toFixed(2)}%`;
                            },
                        },
                    },
                },
                data: yData.three,
            },
        ]
    }

    const opt = Object.assign({}, defaultConfig)
    return opt
}

export default {
    SysPrepositionLineAndBar
}