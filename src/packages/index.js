import * as largeScreenChart from "../components/index"
const { screenAdapter, ChartPanel} = largeScreenChart
var componentsObj  = {
  screenAdapter,
  ChartPanel
}

for(let item in componentsObj){
  componentsObj[item].install = function (Vue){
    Vue.component(item, componentsObj[item]);
  }
}

//自定义组件名
const install = (Vue)=>{
  for(let item in componentsObj){
      Vue.component(`largeScreenChart.${item}`, componentsObj[item]);
  }
  Vue.config.globalProperties.largeScreenChart = largeScreenChart
}


export default {
    screenAdapter,
    ChartPanel,
    install,
    largeScreenChart
}


