
import * as echarts from 'echarts/core'
import {  BarChart,PieChart,LineChart,ScatterChart,GaugeChart,RadarChart ,PictorialBarChart,FunnelChart } from 'echarts/charts'
  import {
    TitleComponent,
    TooltipComponent,
    ToolboxComponent,
    GridComponent,
    LegendComponent,
    DataZoomComponent,
    PolarComponent,
  } from 'echarts/components'
  import {
    CanvasRenderer
  } from 'echarts/renderers'
  
  // 注册组件
  echarts.use(
    [
        TitleComponent, 
        TooltipComponent, 
        GridComponent, 
        BarChart,
        PieChart, 
        CanvasRenderer,
        ToolboxComponent,
        LegendComponent,
        PolarComponent,
        RadarChart,
        LineChart,
        PictorialBarChart,
        ScatterChart,
        GaugeChart,
        FunnelChart,
        DataZoomComponent
    ]
  )

export default echarts